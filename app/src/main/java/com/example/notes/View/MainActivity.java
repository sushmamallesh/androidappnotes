package com.example.notes.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.notes.R;
import com.example.notes.ViewModel.RoomDBNote;
import com.example.notes.Model.ModelNote;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;



//
public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    RecyclerView recyclerView;
    NoteListAdapter noteListAdapter;
    List<ModelNote> modelNotes = new ArrayList<>();
    RoomDBNote database;
    FloatingActionButton floatingActionButton;
    SearchView sv_home;
    ModelNote selectedNote;
    //private List<ModelNote> ModelNotes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyler_home);
        sv_home = findViewById(R.id.sv_home);

        floatingActionButton = findViewById(R.id.fab_add);

        database = RoomDBNote.getInstance(this);

        modelNotes = database.noteDAO().getAll();

        updateRecycle(modelNotes);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NotesTakerActivity.class);
                startActivityForResult(intent, 101);

            }
        });
        sv_home.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return true;
            }
        });

    }

    private void filter(String newText) {

        List<ModelNote> filteredList = new ArrayList<>();
        for (ModelNote singleNote : modelNotes) {
            if (singleNote.getTitle().toLowerCase().contains(newText.toLowerCase()) ||
                    singleNote.getNotes().toLowerCase().contains(newText.toLowerCase())) {
                filteredList.add(singleNote);


            }
        }
        noteListAdapter.filterList(filteredList);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                ModelNote new_modelNote = (ModelNote) data.getSerializableExtra("modelnote");
                database.noteDAO().insert(new_modelNote);
                modelNotes.clear();
                modelNotes.addAll(database.noteDAO().getAll());
                noteListAdapter.notifyDataSetChanged();
            }
        } else if (requestCode == 102) {
            if (resultCode == Activity.RESULT_OK) {
                ModelNote new_modelNote = (ModelNote) data.getSerializableExtra("modelnote");
                database.noteDAO().update(new_modelNote.getID(), new_modelNote.getTitle(), new_modelNote.getNotes());
                modelNotes.clear();
                modelNotes.addAll(database.noteDAO().getAll());
                noteListAdapter.notifyDataSetChanged();
            }
        }
    }

    private void updateRecycle(List<ModelNote> modelNote) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL));
        noteListAdapter = new NoteListAdapter(MainActivity.this, modelNote, notesClickListener);
        recyclerView.setAdapter(noteListAdapter);
    }

    private final NotesClickListener notesClickListener = new NotesClickListener() {
        @Override
        public void onClick(ModelNote modelNote) {
            Intent intent = new Intent(MainActivity.this, NotesTakerActivity.class);
            intent.putExtra("old data", modelNote);
            startActivityForResult(intent, 102);


        }

        @Override
        public void onLongClick(ModelNote modelNote, ConstraintLayout constraintLayout) {
            selectedNote = new ModelNote();
            selectedNote = modelNote;
            showPopup(constraintLayout);
        }
    };

    private void showPopup(ConstraintLayout constraintLayout) {
        PopupMenu popupMenu = new PopupMenu(this, constraintLayout);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.popup);
        popupMenu.show();

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pin:
                if (selectedNote.isPinned()) {
                    database.noteDAO().pin(selectedNote.getID(), false);
                    Toast.makeText(MainActivity.this, "unpinned", Toast.LENGTH_SHORT).show();

                } else {
                    database.noteDAO().pin(selectedNote.getID(), true);
                    Toast.makeText(MainActivity.this, "pinned", Toast.LENGTH_SHORT).show();

                }
                modelNotes.clear();
                modelNotes.addAll(database.noteDAO().getAll());
                noteListAdapter.notifyDataSetChanged();
                return true;

            case R.id.delete:
                database.noteDAO().delete(selectedNote);
                modelNotes.remove(selectedNote);
                noteListAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, "Note deleted", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }

    }
}