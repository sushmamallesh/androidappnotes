package com.example.notes.View;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.notes.Model.ModelNote;

public interface NotesClickListener {
    void onClick(ModelNote modelNote);

    void onLongClick(ModelNote modelNote, ConstraintLayout constraintLayout);
}
