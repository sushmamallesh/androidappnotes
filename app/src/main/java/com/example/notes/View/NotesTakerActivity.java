package com.example.notes.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.notes.Model.ModelNote;
import com.example.notes.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NotesTakerActivity extends AppCompatActivity {

    EditText ettitle, etnotes;
    ImageView iv_save;
    ModelNote modelNote;
    boolean isOld_Note = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_taker);

        ettitle = findViewById(R.id.ettitle);
        etnotes = findViewById(R.id.etnotes);
        iv_save = findViewById(R.id.iv_save);


        modelNote = new ModelNote();
        try {
            modelNote = (ModelNote) getIntent().getSerializableExtra("old data");
            ettitle.setText(modelNote.getTitle());
            etnotes.setText(modelNote.getNotes());
            isOld_Note = true;
        } catch (Exception e) {
            e.printStackTrace();
        }


        iv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = ettitle.getText().toString();
                String note = etnotes.getText().toString();


                if (title.isEmpty()) {
                    Toast.makeText(NotesTakerActivity.this, "please give some title", Toast.LENGTH_LONG).show();
                    return;
                }

                if (note.isEmpty()) {
                    Toast.makeText(NotesTakerActivity.this, "please add some notes", Toast.LENGTH_SHORT).show();
                    return;
                }


                SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm a");
                Date date = new Date();

                if (isOld_Note == false) {
                    modelNote = new ModelNote();
                }

                modelNote.setTitle(title);
                modelNote.setNotes(note);
                modelNote.setDate(formatter.format(date));

                Intent intent = new Intent();
                intent.putExtra("modelnote", modelNote);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String title = ettitle.getText().toString();
        String note = etnotes.getText().toString();

        if (item.getItemId() == R.id.delete1) {
            ettitle.setVisibility(View.GONE);
            etnotes.setVisibility(View.GONE);
        }

        int id = item.getItemId();
        if (item.getItemId() == R.id.save) {
            if (note.isEmpty()) {
                Toast.makeText(NotesTakerActivity.this, "please add some notes", Toast.LENGTH_SHORT).show();

            }
            SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm a");
            Date date = new Date();

            if (!isOld_Note) {
                modelNote = new ModelNote();
            }

            modelNote.setTitle(title);
            modelNote.setNotes(note);
            modelNote.setDate(formatter.format(date));

            Intent intent = new Intent();
            intent.putExtra("modelnote", modelNote);
            setResult(Activity.RESULT_OK, intent);
            finish();


//             Button button = (Button) findViewById(R.id.delete1);
//             button.setOnClickListener(new View.OnClickListener() {
//                 public void onClick(View view) {
//                     ViewGroup parentView = (ViewGroup) view.getParent();
//                     parentView.removeView(view);
//                 }
//             });


//             switch (item.getItemId()){
//                 case R.id.delete1:
//                     Toast.makeText(NotesTakerActivity.this, "Note deleted", Toast.LENGTH_SHORT).show();
//                     return true;
//                 default:
//                     return false;
//             }


//             if(item.getItemId()==R.id.delete1){

//                 ettitle.setVisibility(View.GONE);
//                 etnotes.setVisibility(View.GONE);

//             }
        }
        return true;
    }
}

