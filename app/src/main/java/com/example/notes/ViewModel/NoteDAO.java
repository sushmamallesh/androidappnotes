package com.example.notes.ViewModel;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.notes.Model.ModelNote;

import java.util.List;

@Dao
public interface NoteDAO {

    @Insert(onConflict = REPLACE)
    void insert(ModelNote mobelnote);

    @Query("SELECT * FROM note ORDER BY ID DESC")
    List<ModelNote> getAll();

    @Query("UPDATE note SET title= :title,notes= :notes WHERE ID= :id")
    void update(int id, String title, String notes);

    @Delete
    void delete(ModelNote modelNote);

    @Query("UPDATE note SET pinned=:pin WHERE ID=:id")
    void pin(int id, boolean pin);

}
