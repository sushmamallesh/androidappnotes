package com.example.notes.ViewModel;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.notes.Model.ModelNote;

@Database(entities = ModelNote.class, version = 1, exportSchema = false)
public abstract class RoomDBNote extends RoomDatabase {

    private static RoomDBNote database;
    private static String Database_name = "NOTEAPP";

    public synchronized static RoomDBNote getInstance(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context.getApplicationContext(), RoomDBNote.class, Database_name)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }

    public abstract NoteDAO noteDAO();
}
